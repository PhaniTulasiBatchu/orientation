# What is GitLab
Gitlab is a service that provides remote access to Git repositories. In addition to hosting your code, the services provide additional features designed to help manage the software development lifecycle. These additional features include managing the sharing of code between different people, bug tracking, wiki space and other tools for 'social coding'.
The software was created by Ukrainians Dmitriy Zaporozhets and Valery Sizov.
![](https://miro.medium.com/max/4010/1*j9hbjszo0zXS32yhvSkdAQ.jpeg)

# How to install Gitlabs
It is possible to install and use Gitlab on different operating systems.
[For Windows and Ubuntu](https://www.tutorialspoint.com/gitlab/gitlab_installation.htm)
This gives a clear step-by-step procedure for pleasant and successful installation.

>GitLab is great way to manage git repositories on centralized server. GitLab gives you complete control over your repositories or >projects and allows you to decide whether they are public or private for free.

# Features
* GitLab hosts your (private) software projects for free.
* GitLab is a platform for managing Git repositories.
* GitLab offers free public and private repositories, issue-tracking and wikis.
* GitLab is a user friendly web interface layer on top of Git, which increases the speed of working with Git.
* GitLab provides its own Continuous Integration (CI) system for managing the projects and provides user interface along with other features of GitLab.
![](https://about.gitlab.com/images/single-application/single-application-all-devops.png)

# Advantages
* GitLab provides GitLab Community Edition version for users to locate, on which servers their code is present.
* GitLab provides unlimited number of private and public repositories for free.
* The Snippet section can share small amount of code from a project, instead of sharing whole project.

# Gitlab CI/code
GitLab CI (Continuous Integration) service is a part of GitLab that build and test the software whenever developer pushes code to application. GitLab CD (Continuous Deployment) is a software service that places the changes of every code in the production which results in every day deployment of production.
The following points describe usage of GitLab CI/CD −
* It is easy to learn, use and scalable.
* It is faster system which can be used for code deployment and development.
* You can execute the jobs faster by setting up your own runner (it is an application that processes the builds) with all dependencies which are pre-installed.
* GitLab CI solutions are economical and secure which are very flexible in costs as much as machine used to run it.
* It allows the project team members to integrate their work daily, so that the integration errors can be identified easily by an automated build.
![](https://miro.medium.com/max/5100/1*L1cWY0jwHHmLvkTbHK7Yng.png)

# Forking a project
Forking means creating a duplicate copy of the original project and doing personal changes to it. In otherwords, we can design a template and share it with all the teams so that the basic structure of the project remains same but the content varies.
[Forking Procedure](https://www.tutorialspoint.com/gitlab/gitlab_fork_project.htm)

# WiKi - its the warehouse of knowledge
All the files and important documents related to your project, which give the basic idea about the concepts involved is contained in wiki. For any new joiners or anyone who is going to review your project or anyone who takes your project as a referrence cn first dive into the wiki column and then go through all the necessary concepts and then proceed with the project.
![](https://www.tutorialspoint.com/gitlab/images/wikipage-1.jpg)

# Issues
Issues contain lists, boards, labels, service desk and milestones. These serve their individual purposes. Mainly issues are used to assign tasks to the team members. Details about the tasks to be performed and the way of submission is mentioned here. It is very useful to clearly layout all the tasks along with the requists, so that it will be really easy for the members to follow.
![](https://www.tutorialspoint.com/gitlab/images/create-issue-1.jpg)

# SSH Key
The SSH stands for Secure Shell or Secure Socket Shell used for managing the networks, operating systems and configurations and also authenticates to the GitLab server without using username and password each time. You can set the SSH keys to provide a reliable connection between the computer and GitLab. Before generating ssh keygen, you need to have Git installed in your system.
[steps to create a SSH key](https://www.tutorialspoint.com/gitlab/gitlab_ssh_key_setup.htm)

# Conclusion
Gitlab is one of the modern and smart way of organizing our team projects. It is very user-friendly way of working, which makes everything run in a pleasant manner!

# Credits
[](https://www.tutorialspoint.com/gitlab/index.htm)


