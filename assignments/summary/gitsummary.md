# Git
Git is a distributed version control system, for tracking changes in source code during software development.
It is designed for coordinating work among the programmers, at the same time it can also be used to track the changes in any set of files. Its goal include speed, data integrity,and support for distributed and non-linear work-flows.If you understand what Git is and the fundamentals of how it works, then using Git effectively will probably be much easier for you!

# How to install Git
You will be happy to know that its super easy to install Git
#### For Linux
Open the terminal and type  _sudo apt-get install git_
#### For Windows
[Visit the official website and download](https://git-scm.com/download/win)

# How is Git different in handling its DATA
The major difference between Git and any other VCS (version control system) is the way Git thinks about its _data_.
Conceptually, most of the systems store information as a list or set of files and changes are made to each file over time.Git doesn’t think of or store its data this way. Instead, Git thinks of its data more like a series of snapshots of a miniature filesystem. With Git, every time you commit, or save the state of your project, Git basically takes a picture of what all your files look like at that moment and stores a reference to that snapshot. To be efficient, if files have not changed, Git doesn’t store the file again, just a link to the previous identical file it has already stored. Git thinks about its data more like a stream of snapshots.

![](https://git-scm.com/book/en/v2/images/snapshots.png)

# Security
Git has been designed with the integrity of managed source code as a top priority. The content of the files as well as the true relationships between files and directories, versions, tags and commits, all of these objects in the Git repository are secured with a cryptographically secure hashing algorithm called SHA1. This protects the code and the change history against both accidental and malicious change and ensures that the history is fully traceable.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/SHA-1.svg/300px-SHA-1.svg.png)

With Git, you can be sure you have an authentic content history of your source code.

Some other version control systems have no protections against secret alteration at a later date. This can be a serious information security vulnerability for any organization that relies on software development.

# Version control with Git
Git is, first and foremost, a version control system (VCS). There are many version control systems out there: CVS, SVN, Mercurial, Fossil, and, of course, Git.
Git serves as the foundation for many services, like GitHub and GitLab, but you can use Git without using any other service. This means that you can use Git privately or publicly.
In addition to the benefits of a large talent pool, the predominance of Git also means that many third party software tools and services are already integrated with Git including IDEs, and our own tools like DVCS desktop client Sourcetree, issue and project tracking software, Jira, and code hosting service, Bitbucket.
If you are an inexperienced developer wanting to build up valuable skills in software development tools, when it comes to version control, Git should be on your list.

# How To Create A repository
In your computer, create a folder for your project. Let’s call the project folder _simple-git-demo_.
Go into your project folder and add a local Git repository to the project using the following commands:
_cd simple-git-demo_
_git init_
The _git init_ command adds a local Git repository to the project.

# Branches
Git supports branching which means that you can work on different versions of your collection of files. A branch allows the user to switch between these versions so that he can work on different changes independently from each other.
Branches in Git are local to the repository. A branch created in a local repository does not need to have a counterpart in a remote repository. Local branches can be compared with other local branches and with remote-tracking branches. A remote-tracking branch proxies the state of a branch in another remote repository.

Git supports the combination of changes from different branches. The developer can use Git commands to combine the changes at a later point in time.

![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRR6CPMjkveACx785U0rNIN9XWpvdOpDE3ZmQ&usqp=CAU)

# Credits
[original source](https://www.atlassian.com/git/tutorials/what-is-git)

